# Bienvenue sur le Dépôt de Partage PUBLIC pour l'Enseignement de la Spé NSI

Vous aimez ce projet ?

* Participez activement en publiant des Ressources
* Likez / Ajoutez une étoile au Projet ! :smile:

Ce dépôt **PUBLIC** centralise des Documents **PUBLICS** Partagés par des Enseignants de NSI, à destination de TOUT internaute sur Terre.

:warning: **Ce projet Public est accessible à TOUS les Elèves** :warning:
